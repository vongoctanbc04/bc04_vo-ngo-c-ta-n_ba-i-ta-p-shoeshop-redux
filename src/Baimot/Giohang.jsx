import React, { Component } from 'react'

export default class Giohang extends Component {
    renderTbody=()=>{
       return this.props.gioHang.map((item)=>{
            return <tr>
                <td>{item.name}</td>
                <td>{item.price}</td>
                <td>
                    {""}
                    <img src={item.image} style={{width:50}} alt="" />
                </td>
                <td>
                  <button className='btn btn-warning'>-</button>
                  <span className='mx-5'>{item.soLuong}</span>
                  <button className=' btn btn-primary'>+</button>
                </td>
                <td>
                <button onClick={()=>{
                  this.props.handleRemove(item.id)
                }} className="btn btn-danger">Xoa</button>
                </td>
            </tr>
        })
    }
  render() {
    return (
      <div className="container p-5">
        <table className='table'>
        <thead>
            <tr>
                <th>Ten</th>
                <th>Gia</th>
                <th>Hinh anh</th>
                <th>Số Lượng</th>
            </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
        </table>
        {this.props.gioHang.length == 0 && (<p className='mt-5 text-center text-danger'>Quý khách hãy thêm sản phảm vào giỏ hàng.!</p>)}
      </div>
    
    )
  }
}
