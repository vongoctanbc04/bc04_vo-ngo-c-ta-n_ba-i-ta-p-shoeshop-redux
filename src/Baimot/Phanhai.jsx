import React, { Component } from 'react'
import Phanba from './Phanba'

export default class Phanhai extends Component {
  render() {
    return (
      <div container>
        <div  className="row">
        {this.props.data.map((item,index)=>{
            return <div key={index} className="col-3">
                <Phanba dete={item} handleHang={this.props.handleHang}/>
            </div>
        })}
      </div>
      </div>
    )
  }
}
